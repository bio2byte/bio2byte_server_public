# -*- coding: utf-8 -*-
import os
import tempfile
import subprocess, sys
cwd=os.path.dirname(os.path.abspath(__file__))
if sys.platform == 'linux':
    SEQ2MTX = cwd+'/psipred/bin/linux/seq2mtx'
    PSIPRED = cwd+'/psipred/bin/linux/psipred'
    PSIPASS2 = cwd+'/psipred/bin/linux/psipass2'
elif sys.platform == 'darwin':
    SEQ2MTX = cwd+'/psipred/bin/osx/seq2mtx'
    PSIPRED = cwd+'/psipred/bin/osx/psipred'
    PSIPASS2 = cwd+'/psipred/bin/osx/psipass2'
DATADIR = cwd+'/psipred/data/'
TEMPDIR = cwd+'/psipred/tmp/'
for execpath in [SEQ2MTX,PSIPRED,PSIPASS2]:
    if os.path.exists(execpath):
        os.chmod(execpath, int('777', 8))

def run(input):
    fasta = os.path.basename(input)
    name = fasta.split('.')[-2]
    print('Psipred Running... processing: '+name)
    mtx_file = open(TEMPDIR+name+".mtx", "w")
    seq2mtx = subprocess.run( [SEQ2MTX, input], stdout=mtx_file)
    mtx_file.close()
    ss_file = open(TEMPDIR+name+".ss", "w")
    psipred = subprocess.run( [PSIPRED, TEMPDIR+name+".mtx", DATADIR+'weights.dat', DATADIR+'weights.dat2', DATADIR+'weights.dat3'], stdout=ss_file)
    ss_file.close()
    horiz_file = open(TEMPDIR+name+".horiz", "w")
    psipass2 = subprocess.run( [PSIPASS2, DATADIR+'weights_p2.dat', '1', '1.0', '1.0', TEMPDIR+name+".ss2", TEMPDIR+name+".ss"], stdout=horiz_file)
    horiz_file.close()
