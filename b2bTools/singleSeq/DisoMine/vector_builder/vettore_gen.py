#!/usr/bin/env python2
# -*- coding: utf-8 -*-

prop_sw=35

import string,random,os
from .runpsipred_single import run
### objects ###
amino_index={'A': 1, 'C':2, 'B': -1, 'E': 3, 'D': 4, 'G': 5, 'F': 6, 'I': 7, 'H': 8, 'K': 9, 'M': 10, 'L': 11, 'N': 12, 'Q': 13, 'P': 14, 'S':15, 'R': 16, 'U':-1, 'T': 17, 'W': 18, 'V': 0, 'Y': 19, 'X': -1, 'Z': -1}

class psipred:

	# Directory of this script
	scriptDir = os.path.dirname(os.path.realpath(__file__))

	def __init__(self,single_sequence=True):

		self.tmpfold = os.path.join(self.scriptDir, 'psipred/tmp/')

		if single_sequence:
			pass
		else:
			raise(NotImplementedError)

	def predict(self,seq):
		name=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(7))
		f=open(self.tmpfold+name+'.fasta','w')
		pred=[]
		f.write('>seq\n'+seq)
		f.close()
		run(self.tmpfold+name+'.fasta')
		f=open(self.tmpfold+name+'.ss2').readlines()
		for i in f[2:]:
			a=i.strip().split()
			if len(a)==0:
				continue
			else:
				pred+=[[float(a[3]),float(a[4]),float(a[5])]]
		os.system('rm {}/*.horiz'.format(self.tmpfold))
		os.system('rm {}/*.ss'.format(self.tmpfold))
		os.system('rm {}/*.ss2'.format(self.tmpfold))
		return pred

def build_vector(seq,dmPredictions,TYPE=None,sw=None,nomeseq=None):
	vet=[]
	last=0
	seq_nogap=seq.replace('-','')
	for i in range(len(seq)):
		vet+=[[]]
	nfeatures=0
	for curr_fea in TYPE.split(','):
		if curr_fea.startswith('dyna') or curr_fea == 'ef':

			if curr_fea == 'dyna_coil':
				v_dyna = [dmPredictions['coil']]
			elif curr_fea == 'dyna_sheet':
				v_dyna = [dmPredictions['sheet']]
			elif curr_fea == 'dyna_helix':
				v_dyna = [dmPredictions['helix']]
			elif curr_fea == 'dyna_side':
				v_dyna = [dmPredictions['sidechain']]
			elif curr_fea == 'dyna_back':
				v_dyna = [dmPredictions['backbone']]
			elif curr_fea == 'ef':
				v_dyna = [dmPredictions['earlyFolding']]

			effett=0
			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]
						else:
							vet[i]+=[v_dyna[0][effett+s][1]]
					effett+=1
					last=i

		elif 'psipred' == curr_fea:
			D = psipred()
			v_psipred=D.predict(seq_nogap)
			effett=0
			#v_dyna=numpy.gradient(numpy.array(v_dyna))

			for i in range(len(vet)):
				if seq[i]!='-':
					#print i
					for s in range(-sw,sw+1):
						if effett+s<0:
							vet[i]+=[0]*3
						elif effett+s>=len(seq_nogap):
							vet[i]+=[0]*3
						else:
							vet[i]+=v_psipred[effett+s]
					effett+=1
					last=i

		else:
			print((curr_fea,'IS NOT A FEATURE'))
			assert False

	for i in range(len(seq)):
		if seq[i]=='-' or seq[i]=='.':
			vet[i]+=['-']*nfeatures

	return vet
