#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  standalone.py
#
#  Copyright 2017 Gabriele Orlando <orlando.gabriele89@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from b2bTools.singleSeq.DisoMine.vector_builder.vettore_gen import build_vector
from b2bTools.singleSeq.DisoMine.torch_NN_gru_80AUC_prova import nn_pytorch
import numpy as np
import sys
import pathlib

def standalone(seqList,dmPredictions): #input_obj is now a list of (seqId,seq) tuples, is faster. dmPredictions is the existing dynaMine/EFoldMine preds, done earlier

	# These are the features used in the final version
	features='dyna_back,psipred,ef,dyna_side'

	# Directory of this script
	scriptDir = str(pathlib.Path(__file__).parent.absolute())

	# This is the window size used
	window=4

	verbose=0
	def check_sequences(seqList):

		for (seqId,seq) in seqList:
			if not seq.isalpha():
				return {'error':'invalid char in sequence '+i}
			#if len(seq)>3000:
				#print len(seqs[i])
			#	return {'error':'sequence '+i+' too long, maximum length is 3000 amino acids'}
			if len(seq)<20:
				#print len(seqs[i])
				return {'error':'sequence '+seqId+' too short, minimum length is 20 amino acids'}
		return True

	"""
	def clean_psipred_tmp():
		print('Running Cleaner')
		TEMPDIR = scriptDir+'/vector_builder/psipred/tmp/'
		for filename in os.listdir(TEMPDIR):
			file_path = os.path.join(TEMPDIR, filename)
			try:
				if os.path.isfile(file_path) or os.path.islink(file_path):
					os.unlink(file_path)
				elif os.path.isdir(file_path):
					shutil.rmtree(file_path)
			except Exception as e:
					print('Failed to delete %s. Reason: %s' % (file_path, e))
	"""

	def load_model():
		sys.path.append(scriptDir)
		mod=nn_pytorch(cuda=False)
		mod.load_model('{}/gru80_final.mtorch'.format(scriptDir))
		scaler=None
		return mod,scaler

	def format_output(disorder,seqList,dmPredictions):
		results = {}
		for (protID,sequence) in seqList:
			results[protID] = []
			for i in range(len(sequence)):
				results[protID].append((sequence[i],np.float64(disorder[protID][i])))

			dmPredictions[protID]['disoMine'] = results[protID]

		return results

	def predict(seqList,dmPredictions,crunch=100):

		check=check_sequences(seqList)
		if check!=True:
			return check

		results_dict={}
		cont=0
		dyna={}
		side={}
		ef={}

		numSeqs = len(seqList)

		# Doing 100 sequences at a time with disomine
		for i in range(0,numSeqs+1,crunch):
			if i+crunch>numSeqs:
				end=numSeqs
			else:
				end=i+crunch
			vet_crunch=[]
			if verbose>=1:
				print('starting crunch',cont)

			targets = []
			for (target,seq) in seqList[i:end]:
				targets.append(target)
				if verbose>=2:
					print('\tstarting target:',target)
				v=np.array(build_vector(seq,dmPredictions[target],TYPE=features,sw=window))
				#print v[]
				dyna[target]=v[:,3]
				ef[target]=v[:,32]
				side[target]=v[:,39]
				vet_crunch+=[v]
				#print np.array(build_vector(a[target],TYPE=TYPE,sw=ws)).shape
			cont+=1
			predictions=model.decision_function(vet_crunch)
			assert len(predictions)==len(vet_crunch)

			for target,res in zip(targets,predictions):
				#print res
				#res=np.reshape(res,(len(res),1))

				#res=scaler.transform(res)

				#res=np.reshape(res,(len(res)))
				for k in range(len(res)):
					if res[k]<0:
						res[k]=0.0
					elif res[k]>1:
						res[k]=1.0
				results_dict[target]=res

		# Adding results to existing information as well here, but output is just disoMine
		results=format_output(results_dict,seqList,dmPredictions)

		return results

	# This is the main code for this def, calling other defs inside this one
	#clean_psipred_tmp()
	model,scaler=load_model()
	results=predict(seqList,dmPredictions)
	return results

def main(args):
	#print standalone("example.fasta")
	a=leggifasta('input_files_examples/example_toy.fasta')
	a['extra_predictions']=False
	print(standalone(a))
	#from memory_profiler import memory_usage
	#mem_usage = memory_usage(standalone,interval=0.01)
	#print('Memory usage (in chunks of .1 seconds): %s' % mem_usage)
	#print('Maximum memory usage: %s' % max(mem_usage))
	#cProfile.run('standalone("example.fasta")')

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
