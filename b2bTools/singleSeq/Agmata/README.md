### INSTALLATION ###
AgMata have been tested on Linux machines only. It may require additional installation steps in order to work on other operating systems. If you need support, please contact gabriele.orlando@vub.be or wim.vranken@vub.be.

Besides the dependencies (see below),  AgMata can be run directly from the  AgMata.py file as:

python AgMata.py [args]

### DEPENDENCIES ###

the following python packages are required:

- python2.7
- scipy (tested with version 0.19.1)
- numpy (tested with version 1.14.3)
- sklearn (tested with version 0.19.1)

all these packages are available using pip or anaconda.

### USAGE ###

The tool takes as input FASTA files.

To use the tool, run

python  AgMata.py [options]

to test the example input, run

python  agmata.py -i input_example/example_toy.fasta

The tool should generate the results in a couple of seconds.

-h --> show the help
-i --> input FASTA file file
-o --> output file

### OUTPUT FORMAT ###

the output is made of three columns: the first one contains the residue name, the second one the  AgMata score and the third one the binary prediction. 
