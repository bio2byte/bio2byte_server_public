## MSA based tools

This directory contains tools that require a multiple sequence alignment (MSA) as input.
This MSA can be provided by the user, or can be automatically provided by an external bioinformatics
API.