import sys
import os
sys.path.append('/'.join(os.getcwd().split('/')[:-2]))  # Append to path
from b2bTools.multipleSeq.Predictor import MineSuiteMSA
import argparse
import json



def parse_input():
  parser = argparse.ArgumentParser(description="Choose the input_examples and output file")
  parser.add_argument('-i', metavar='input_file.txt', required=True,
                      help='Your input MSE file, with wich you want to obtain predictions from DynaMine, DisoMine, EFoldMine and Agmata')
  parser.add_argument('-o', metavar='output_file', required=True,
                      help='Output name for your predictions. Do not include any file format, as we generate multiple files.')

  args = parser.parse_args()

  return args


if __name__ == '__main__':
  arguments = parse_input()
  input_file = arguments.i

  ms = MineSuiteMSA()
  ms.predictSeqsFromMSA(input_file)

  # 1 All the predictions for the individual sequences
  # print(ms.allPredictions)

  # 2 Same but now mapped to full MSA, gaps are None, not yet implemented
  ms.predictAndMapSeqsFromMSA(input_file)
  with open("out/" + arguments.o + "_alignment.json", 'w') as outfile:
    print("Generating predictions and alignments...")
    json.dump(ms.allAlignedPredictions, outfile, indent=4)

  # 3 Same but now mapped to reference sequence ID in MSA, gaps are None
  # ms.predictAndMapSeqsFromMSA(input_file, dataRead=True)
  # print(json.dumps(ms.allAlignedPredictions, indent=4))

  # 4 Same but now mapped to full MSA, distributions of scores.
  ms.getDistributions()
  with open("out/" + arguments.o + "_distributions.json", 'w') as outfile:
    "Calculating distributions..."
    json.dump(ms.alignedPredictionDistribs, outfile, indent=4)

  print("Done! Find your output files in the \"out/\" directory")


