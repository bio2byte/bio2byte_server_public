import sys
import os
sys.path.append('/'.join(os.getcwd().split('/')[:-2]))  # Append to path
from b2bTools.singleSeq.Predictor import MineSuite
import argparse


def parse_input():
    parser = argparse.ArgumentParser(description="Choose the input_examples and output file")
    parser.add_argument('-i', metavar='input_file.fasta', required=True, help='Your input fasta file, with wich you want to obtain predictions from DynaMine, DisoMine, EFoldMine and Agmata')
    parser.add_argument('-o', metavar='output_file.json', required=True, help='Output name for your predictions, we will include the extension for you.')

    args = parser.parse_args()

    if args.i.split('.')[-1] != 'fasta':
        sys.exit('Error: The input_examples file must be a fasta file')

    return args


if __name__ == '__main__':
    arguments = parse_input()
    ms = MineSuite()
    # input_file = "./input_examples/input_example.fasta"
    input_file = arguments.i
    ms.predictFromFasta(input_file)
    # print(ms.getAllPredictionsJson('test'))

    with open("out/" + arguments.o + '.json', 'w') as file:
        print('Writting output...')
        file.write(ms.getAllPredictionsJson('my_label'))
        print('Done! Find your output file in out/' + arguments.o + '.json')