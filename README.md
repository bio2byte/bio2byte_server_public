# README

This repository contains the software employed in the [bio2Byte tools online suite](https://www.bio2byte.be/b2btools/).

### References

1. Luciano Porto Kagami, Gabriele Orlando, Daniele Raimondi, Francois Ancien, Bhawna Dixit, Jose Gavaldá-García, Pathmanaban Ramasamy, Joel Roca-Martínez, Konstantina Tzavella, Wim Vranken, b2bTools: online predictions for protein biophysical features and their conservation, Nucleic Acids Research, Volume 49, Issue W1, 2 July 2021, Pages W52–W59, https://doi.org/10.1093/nar/gkab425

### What is this repository for?

* Obtain protein biophysical properties predictions
* Obtain an MSA alignment of protein's biophysical characteristics
* Version 1.0.0

### How do I get set up? 

* This package is currently only works on Linux and MacOS. 
Future versions will include Windows implementations, but for now we suggest Windows users to use of virtual machines or the Windows Subsystem for Linux (WSL).

* Create an anaconda environment with python 3.7

`conda create --name envname python=3.7`

* Install the dependencies provided in `requirements.txt` on the top directory

`conda install --file requirements.txt`

* Go to the directory `b2bTools/run/` and run your desired script
* Execute the scripts with the `-h` flag for more information

### License
This project is licensed under the MIT license, a short and simple permissive license with conditions only requiring preservation of copyright and license notices. Licensed works, modifications, and larger works may be distributed under different terms and without source code.

**Permissions**:

* Commercial use
* Modification
* Distribution
* Private use

**Limitations**:

* Liability
* Warranty

**Conditions**:

* License and copyright notice

Be provided with our license on this [link](https://bitbucket.org/bio2byte/bio2byte_server_public/src/master/LICENSE.md). 

### Who do I talk to?

* This repository belongs to bio2Byte, lead by Prof. Wim Vranken
* For questions or inquiries, please contact Prof. Wim Vranken at [Wim.Vranken@vub.be](mailto:Wim.Vranken@vub.be)